"""
Программа выполняет следующие действия:
сигнатура для запуска:
python about_the_chess_horse.py *1st_koords* *2nd_koords* *mode*
- mode = move: Находит минимальное количество шагов, необходимое коню,
чтобы добраться из одной клетки в другую
- mode = collision: Находит минимальное количество шагов, чтобы встретились 2 коня
"""
from collections import deque
import argparse
from graph import graph


def knight_move(start: tuple, finish: tuple) -> int:
    """
    Находит минимальное количество шагов, необходимое коню.
    На вход передается 2 кортежа с координатами клеток,
    функция находит кратчайший путь и считает, проделанное количество шагов
    для шахматного коня.
    :param start: tuple с координатами начальной клетки.
    :param finish: tuple с координатами конечной клетки.
    :return: число int - кол-во шагаов.
    :raises: None
    Examples:
    >>> knight_move((5, 6),(2, 3))
    2
    >>> knight_move((1, 1),(8, 8))
    6
    """

    def btf(start_help, finish_help, graph_help):
        """Вспомогательная функция для поиска в ширину"""
        queue = deque([start_help])
        visited_help = {start_help: None}

        while queue:
            cur_node_help = queue.popleft()
            if cur_node_help == finish_help:
                break

            next_nodes = graph_help[cur_node_help]
            for next_node in next_nodes:
                if next_node not in visited_help:
                    queue.append(next_node)
                    visited_help[next_node] = cur_node_help
        return visited_help

    visited = btf(start, finish, graph)
    cur_node = finish
    cnt = 0
    while cur_node != start:
        cur_node = visited[cur_node]
        cnt += 1
    return cnt


def knights_collision(first: tuple, second: tuple) -> int:
    """
    Находит минимальное количество шагов, необходимое для встречи 2ух коней.
    На вход передается 2 кортежа с координатами клеток,
    функция находит минимальное количестов шагов, чтобы встретились 2 коня,
    за один ход принимается ход стразу 2ух фигур, но если необходимо
    нечетное количестов шагов, то на посленднем ходу может сходить 1 конь.
    :param first: tuple с координатами первого коня.
    :param second: tuple с координатами второго коня.
    :return: число int - кол-во шагаов.
    :raises: None
    Examples:
    >>> knights_collision((1, 1),(1, 2))
    2
    >>> knights_collision((1, 1),(8, 8))
    3
    """
    if knight_move(first, second) % 2 == 0:
        return knight_move(first, second) // 2
    return knight_move(first, second) // 2 + 1


def main():
    """Точка входа, командный интерфейс"""
    parser = argparse.ArgumentParser()
    parser.add_argument("start_or_first", type=str)
    parser.add_argument("finish_or_second", type=str)
    parser.add_argument("func", type=str)
    args = parser.parse_args()

    if args.start_or_first.isdigit() and args.finish_or_second.isdigit():
        if 11 <= int(args.start_or_first) <= 88 and 11 <= int(args.finish_or_second) <= 88:
            if args.func == 'move':
                start_cmd = (int(args.start_or_first[0]), int(args.start_or_first[1]))
                finish_cmd = (int(args.finish_or_second[0]), int(args.finish_or_second[1]))
                print(f'Необходимое количество ходов: {knight_move(start_cmd, finish_cmd)}')
            elif args.func == 'collision':
                first_cmd = (int(args.start_or_first[0]), int(args.start_or_first[1]))
                second_cmd = (int(args.finish_or_second[0]), int(args.finish_or_second[1]))
                print(f'Минимальное количество ходов для встречи: {knights_collision(first_cmd, second_cmd)}\n'
                      f'Один ход - ход сразу двух коней, но на последнем ходу один конь может остаться на месте')
            else:
                print('Вы что-то делаете не так!')
        else:
            print('Вы что-то делаете не так!')
    else:
        print('Вы что-то делаете не так!')


if __name__ == '__main__':
    main()
